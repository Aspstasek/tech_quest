# Techquest data loader 

Initial project goal was to create a service that loads data from link and uploads its content into two tables,
report_input and data_error, based on data validation process

## Technologies used 
1. As framework i choose ASGI for it's lightweight and simple functionality(routing as decorators, etc)
2. Database Postgresql for it's simple sintax, good python support library
3. Python3.7. I want to use 3.8 because of it's from-the-box type hinting but sometimes good-old-known is better
4. As far as service is pretty simple, for Docker base image 3.7-slim-buster was chosen

## Interaction
Interaction order is based on REST scheme
Main route placed in '/' you just send the json ```{"link":{download_link}``` to ```http://host:port``` and in response
 you will receive how much data was loaded into each table based on validation

## Few keynotes on downsides and etc

1. db module is far from good realisation(not so DRY)
2. Also in db *host*, *dbname*, *user* and *password* is getting from environment variables of Dockerfile
3. As far as no data was given about providing database, it assumed that database already given(tested localy from another Docker container)
4. Cause `user` is reserved keyword in Postgres, table field `user` was renamed to `user_id`
6. Error handling on server side may need some improvements
7. Container linkage in bridge network done by --link flag to prepared docker-postgres container. More precise way will
be docker-compose file creation
8. For more clean deployment Docker was changed to multi-stage build
