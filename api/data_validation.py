from typing import Dict, Any
import json
from datetime import datetime


class DataValidator:
    def __init__(self):
        self.fields_validation_dict = {
            "user": self._is_int,
            "ts": self._is_date,
            "context": self._is_json,
            "ip": self._is_str
        }

    @staticmethod
    def _is_int(key: str, num: Any) -> int:
        try:
            return int(num)
        except ValueError:
            raise Exception(f"{key} filed is not integer")

    @staticmethod
    def _is_date(key: str, date: Any) -> datetime.date:
        try:
            return datetime.fromtimestamp(date)
        except (TypeError, ValueError):
            raise Exception(f"{key} field have incorrect EPOCH format?")

    @staticmethod
    def _is_str(key: str, obj: Any) -> str:
        try:
            return str(obj)
        except TypeError:
            raise Exception(f"{key} field in incorrect string format")

    @staticmethod
    def _is_json(key: str, fileobject: Any) -> json:
        try:
            return json.dumps(fileobject)
        except (TypeError, ValueError):
            raise Exception(f"{key} field have incorrect json format")

    def validate(self, data: str, date_from_file: str) -> Dict:
        result_of_validation, validated_values = dict(), list()
        try:
            json_data = json.loads(data)
            for k, v in json_data.items():
                validated_values.append(self.fields_validation_dict[k](k, v))
                result_of_validation.update(
                    {"Success": True, "Values": validated_values})
        except Exception as e:
            # There may be a more elegant\Pythonc way to handle this, but it don't come into my mind right away
            api_report = "Validation error"
            api_date = date_from_file
            row_text = data.decode()
            error_text = ",".join(e.args)
            ins_ts = datetime.now()
            result_of_validation.update({"Success": False,
                                         "Values": [api_report, api_date, row_text, error_text, ins_ts]})
        return result_of_validation
