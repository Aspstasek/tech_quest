import gzip

from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import JSONResponse

from data_loader import load_data_from_url, delete_file
from data_validation import DataValidator
from db import insert_validated, insert_error

validator = DataValidator()
app = Starlette()


@app.route("/")
async def main(request: Request):
    result = dict()
    counter_success, counter_error = 0, 0
    url = (await request.json())["url"]
    file_res = load_data_from_url(url)
    if file_res["Success"]:
        for piece in gzip.open(file_res["File"], "r").readlines():
            #TODO Not so good method for date passing, needs reworking
            date_from_file = file_res["File"].replace(".json.gz", "").replace("input-", "")
            validated_data = validator.validate(piece, date_from_file)
            if validated_data["Success"]:
                counter_success += 1
                insert_validated(validated_data["Values"])
            else:
                counter_error += 1
                insert_error(validated_data["Values"])
    result.update({"Files loaded into db": counter_success, "Files loaded into error table": counter_error})
    delete_file(file_res["File"])
    return JSONResponse(result)
