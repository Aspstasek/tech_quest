from endpoints.load_and_validate import main
from starlette.applications import Starlette


def create_app():
    app = Starlette()
    app.add_route("/", main, methods=["POST"])
    return app
