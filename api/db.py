import psycopg2
from envparse import env

dbname, host = env("DBNAME", default='postgres'), env("HOST", default='postgres-docker')
user, password = env("DBUSER", default='postgres'), env("PASSWORD", default='postgres')


def create_connection():
    conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host)
    return conn.cursor(), conn


def insert_validated(*values: list):
    cursor, conn = create_connection()
    insert_query = """INSERT INTO report_input (user_id,ts,context,ip) VALUES ( %s, %s, %s, %s)"""
    cursor.execute(insert_query, *values)
    conn.commit()


def insert_error(*values: list):
    cursor, conn = create_connection()
    insert_query = """INSERT INTO data_error (api_report,api_date,row_text,error_text,ins_ts) VALUES (%s, %s, %s, %s, %s)"""
    cursor.execute(insert_query, *values)
    conn.commit()
