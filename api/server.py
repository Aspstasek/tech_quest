import uvicorn
from create_app import create_app
from envparse import env


class Config:

    def __init__(self):
        self.app_host = env("ASGI_HOST", default="127.0.0.1")
        self.app_port = env.int("ASGI_APP_PORT", default=80)


config = Config()
app = create_app()

if __name__ == "__main__":
    uvicorn.run(app, host=config.app_host, port=config.app_port)
