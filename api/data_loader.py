import requests
import os


def load_data_from_url(url):
    response = requests.get(url, stream=True)
    target_path = url.split("/")[-1]
    if response.status_code == 200:
        with open(target_path, "wb") as f_out:
            f_out.write(response.raw.read())
        return {"Success": True,
                "File": target_path}
    else:
        return {"Success": False,
                "Err_code": response.status_code,
                "Err_msg": response.text}


def delete_file(path):
    os.remove(path)
