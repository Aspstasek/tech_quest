requests==2.24.0
starlette==0.13.6
psycopg2-binary==2.8.5
gunicorn==20.0.4
uvicorn==0.11.8
envparse==0.2.0