FROM python:3.7-slim-buster
ENV PYTHONBUFFERED = 1
WORKDIR /wheels
COPY ["requirements.txt","/wheels/requirements/"]
RUN pip install -U pip \
    && pip wheel -r ./requirements/requirements.txt
COPY . /wheels

COPY ["./", "/"]

RUN pip install -r /wheels/requirements/requirements.txt\
    -f /wheels\
    && rm -rf /wheels \
    && rm -rf /root/.cache/pip/* \
    && chmod +x /api/server.py
WORKDIR /api/
CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "80"]
